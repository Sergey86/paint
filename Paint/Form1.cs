﻿using Paint.Shapes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Paint
{
    enum ShapeType { Line, Circle, Rectangle, Curve }

    public partial class PaintForm : Form
    {
        private Pen pen = new Pen(Color.Black);
        private ShapeType selectedShapeType;
        private Shape currentShape;
        private Dictionary<Guid, Shapes.Shape> shapes = new Dictionary<Guid, Shapes.Shape>();

        public PaintForm()
        {
            InitializeComponent();
        }

        #region Canvas handlers

        private void CanvasMouseDown(object sender, MouseEventArgs e)
        {
            currentShape = CreateShape(selectedShapeType, e.Location);
        }

        private void CanvasMouseMove(object sender, MouseEventArgs e)
        {
            if (currentShape == null) return;

            currentShape.UpdateEndPoint(e.Location);

            // Remove all existing shapes from the canvas and initiate repaint event
            Canvas.Invalidate();
        }

        private void CanvasMouseUp(object sender, MouseEventArgs e)
        {
            SaveShape(currentShape);
            currentShape = null;
            //Canvas.Invalidate();
        }

        #endregion

        private void OnCanvasPaint(object sender, PaintEventArgs e)
        {
            // Draw everything anew, current shape also
            if (currentShape != null)
            {
                currentShape.Draw(e.Graphics);
            }

            foreach (var shape in shapes)
            {
                shape.Value.Draw(e.Graphics);
            }
        }

        private Shape CreateShape(ShapeType type, Point startPoint)
        {
            Shape shape;

            switch (selectedShapeType)
            {
                case ShapeType.Line:
                    shape = new Line(startPoint, new Pen(pen.Color));
                    break;

                case ShapeType.Rectangle:
                    shape = new Shapes.Rectangle(startPoint, new Pen(pen.Color));
                    break;

                case ShapeType.Circle:
                    shape = new Circle(startPoint, new Pen(pen.Color));
                    break;

                case ShapeType.Curve:
                    shape = new Curve(startPoint, new Pen(pen.Color));

                    break;

                default:
                    throw new Exception("Unknown shape type");
            }

            return shape;
        }

        private void ButtonLineClick(object sender, EventArgs e)
        {
            selectedShapeType = ShapeType.Line;
        }

        private void ButtonCircleClick(object sender, EventArgs e)
        {
            selectedShapeType = ShapeType.Circle;
        }

        private void ButtonRectangleClick(object sender, EventArgs e)
        {
            selectedShapeType = ShapeType.Rectangle;
        }

        private void ButtonCurveClick(object sender, EventArgs e)
        {
            selectedShapeType = ShapeType.Curve;
        }

        private void SaveShape(Shape shape)
        {
            shapes.Add(Guid.NewGuid(), shape);
        }

        private void ColorSelectionClick(object sender, EventArgs e)
        {
            colorMenu.ShowDialog();
            pen.Color = colorMenu.Color;
        }


        private void NewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (shapes.Count > 0)
            {
                shapes.Clear();
                Canvas.CreateGraphics().Clear(Color.White);
            }
        }

        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog OpenFile = new OpenFileDialog();
            OpenFile.Filter = "bmp (*.bmp)|*.bmp|jpeg (*.jpeg)|*.jpeg|png (*.png)|*.png|tiff (*.tiff)|*.tiff";
            if (OpenFile.ShowDialog() == DialogResult.OK)
            {
                Canvas.Image = Image.FromFile(OpenFile.FileName);
            }
        }

        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.DefaultExt = "jpeg";
            saveFile.Filter = "bmp (*.bmp)|*.bmp|jpeg (*.jpeg)|*.jpeg|png (*.png)|*.png|tiff (*.tiff)|*.tiff";
            saveFile.RestoreDirectory = true;
            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                Bitmap bitmap = new Bitmap(Canvas.Width, Canvas.Height);
                using (Graphics graphics = Graphics.FromImage(bitmap))
                {
                    graphics.Clear(Color.White);
                    foreach (var shape in shapes)
                    {
                        shape.Value.Draw(graphics);
                    }
                    bitmap.Save(saveFile.FileName);
                }
            }
        }
    }
}