﻿using System.Collections.Generic;
using System.Drawing;

namespace Paint.Shapes
{
    class Curve : Shape
    {
        List<Point> curvePoints;

        public Curve(Point point1, Pen pen)
            : base(point1, pen)
        {
            curvePoints = new List<Point>() { point1, point1 };
        }

        public override void Draw(Graphics graphics)
        {
            graphics.DrawCurve(Pen, curvePoints.ToArray());
        }

        public override void UpdateEndPoint(Point point)
        {
            curvePoints.Add(point);
        }
    }
}
