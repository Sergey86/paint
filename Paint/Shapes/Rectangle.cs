﻿using System.Drawing;

namespace Paint.Shapes
{
    public class Rectangle : Shape
    {
        public Rectangle(Point point1, Pen pen)
            : base(point1, pen)
        {
        }

        public override void Draw(Graphics graphics)
        {
            if (Point1.X > Point2.X)
            {
                graphics.DrawRectangle(Pen, new System.Drawing.Rectangle(Point2.X, Point1.Y, Point1.X - Point2.X, Point2.Y - Point1.Y));
            }
            if (Point1.Y > Point2.Y)
            {
                graphics.DrawRectangle(Pen, new System.Drawing.Rectangle(Point1.X, Point2.Y, Point2.X - Point1.X, Point1.Y - Point2.Y));
            }
            if (Point1.X > Point2.X || Point1.Y > Point2.Y)
            {
                graphics.DrawRectangle(Pen, new System.Drawing.Rectangle(Point2.X, Point2.Y, Point1.X - Point2.X, Point1.Y - Point2.Y));
            }
            else
                graphics.DrawRectangle(Pen, new System.Drawing.Rectangle(Point1.X, Point1.Y, Point2.X - Point1.X, Point2.Y - Point1.Y));
        }
    }
}
