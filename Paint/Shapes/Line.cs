﻿using System.Drawing;

namespace Paint.Shapes
{
    public class Line : Shape
    {
        
        public Line(Point point1, Pen pen)
            : base(point1, pen)
        {
        }

        public override void Draw(Graphics graphics)
        {
            graphics.DrawLine(Pen, Point1, Point2);
        }
    }
}
