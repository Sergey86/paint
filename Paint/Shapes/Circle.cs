﻿using System.Drawing;

namespace Paint.Shapes
{
    public class Circle : Shape
    {
        public Circle(Point point1, Pen pen)
            : base(point1, pen)
        {
        }

        public override void Draw(Graphics graphics)
        {
            graphics.DrawEllipse(Pen, Point1.X, Point1.Y, Point2.X - Point1.X, Point2.Y - Point1.Y);
        }
    }
}
