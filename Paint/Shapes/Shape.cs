﻿using System.Drawing;

namespace Paint.Shapes
{
    public abstract class Shape
    {
        public Point Point1 { get; protected set; }
        public Point Point2 { get; protected set; }
        public Pen Pen { get; set; }        
        
        public abstract void Draw(Graphics graphics);

        public virtual void UpdateEndPoint(Point point)
        {
            Point2 = point;
        }

        public Shape(Point startPoint,  Pen pen)
        {
            Point1 = startPoint;
            Pen = pen;            
        }
    }
}
